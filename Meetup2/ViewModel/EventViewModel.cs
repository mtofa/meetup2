﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Web;
using Meetup2.Models;

namespace Meetup2.ViewModel
{
    public class EventViewModel
    {
        public int MeetupId { get; set; }
        [Required]
        public string Title { get; set; }


        public MeetUpGroup MeetUpGroups { get; set; }

        [Required(ErrorMessage = "Please select a Meetup Group")]
        public int MeetUpGroupId { get; set; }

        [Required]
        public string Location { get; set; }

       
        [ValidateMeetupDate]
        public string EventDateTime { get; set; }

        public string Details { get; set; }

        public string DispalyTitle { get; set; }

        public EventViewModel()
        {
            MeetupId = 0;
        }

        public DateTime EventDate()
        {

            return DateTime.ParseExact(EventDateTime, "dd MMMM yyyy - hh:mm tt", CultureInfo.InvariantCulture);
            //return DateTime.Parse(string.Format("{0},{1}", Date, Time));
        }

        public string FormAction()
        {
            return (MeetupId == 0) ? "Create" : "Update";
        }
    }

    public class ValidateMeetupDate : ValidationAttribute
    {


        //public ValidateMeetupDate():base("Meetup date time is invalid")
        //{
            
        //}

        //public bool Isvalid(object value) {};

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {

            DateTime date;
            var inputDate = Convert.ToString(value);

            if (string.IsNullOrEmpty(inputDate))
                return new ValidationResult("Meetup Date and Time is required");
            //date =DateTime.ParseExact(inputDate, 
            //    "dd MMMM yyyy - hh:mm tt", CultureInfo.InvariantCulture);
            var isValidDate = DateTime.TryParseExact(inputDate,
            "dd MMMM yyyy - hh:mm tt",
            CultureInfo.CurrentCulture,
            DateTimeStyles.None,
            out date
            );


            if (isValidDate && date > DateTime.Now)
            {
                return ValidationResult.Success;
                
            }
            return new ValidationResult("Please enter a future date");

        }


    }

    //public class ValidateTime : ValidationAttribute
    //{
    //    //public & bool
    //    public override bool IsValid(object value)
    //    {

    //        DateTime time;
    //        var isValid = DateTime.TryParseExact(Convert.ToString(value),
    //            "HH:mm",
    //            CultureInfo.CurrentCulture,
    //            DateTimeStyles.None,
    //            out time
    //            );

    //        return (isValid);

    //    }


    //}
}