using System.Linq;
using Meetup2.Models;

namespace Meetup2.ViewModel
{
    public class EventShowViewModel
    {
        public MeetUp MeetUp { get; set; }
        public ILookup<int,Attendance> AttandanceLookup { get; set; }

    }
}