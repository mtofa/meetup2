﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Meetup2.Models;

namespace Meetup2.ViewModel
{
    public class HomeViewModel
    {
        public IEnumerable<MeetUp> Events { get; set; }
    }
}