using System.Collections.Generic;
using Meetup2.Models;

namespace Meetup2.ViewModel
{
    public class MyAttandanceViewModel
    {
        public IEnumerable<Attendance> Attendance;
        public IEnumerable<MeetUp> MeetUps;
    }
}