﻿using Meetup2.Models;
using System.Collections.Generic;

namespace Meetup2.ViewModel
{
    public class GroupViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string HomeTown { get; set; }
        public IEnumerable<MeetUp> MeetUp { get; set; }
        public ApplicationUser Organizer { get; set; }
        public bool ShowButton { get; set; }

        public GroupViewModel()
        {
            ShowButton = true;
        }

    }
}