﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Meetup2.Models;
using Meetup2.Repositories;

namespace Meetup2.Persistence
{
    public class UnitOfWork : IUnitOfWork
    {
        private  ApplicationDbContext _context;
        public IMeetupRepository MeetupRepository { get; private set; }
        public IMeetupGroupRepository MeetupGroupRepository { get; private set; }
        public IAttandanceRepository AttandanceRepository { get; private set; }

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
            MeetupRepository = new MeetupRepository(_context);
            MeetupGroupRepository = new MeetupGroupRepository(context);
            AttandanceRepository = new AttandanceRepository(context);
            
        }

        public void Complete()
        {
            //_context.SaveChanges();
        }
    }
}