﻿using Meetup2.Repositories;

namespace Meetup2.Persistence
{
    public interface IUnitOfWork
    {
        IAttandanceRepository AttandanceRepository { get; }
        IMeetupGroupRepository MeetupGroupRepository { get; }
        IMeetupRepository MeetupRepository { get; }

        void Complete();
    }
}