namespace Meetup2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Attendances",
                c => new
                    {
                        MeetUpId = c.Int(nullable: false),
                        ParticipantId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.MeetUpId, t.ParticipantId })
                .ForeignKey("dbo.MeetUps", t => t.MeetUpId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.ParticipantId, cascadeDelete: true)
                .Index(t => t.MeetUpId)
                .Index(t => t.ParticipantId);
            
            CreateTable(
                "dbo.MeetUps",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        DateTime = c.DateTime(nullable: false),
                        Location = c.String(),
                        Details = c.String(),
                        MeetUpGroupId = c.Int(nullable: false),
                        IsCancled = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MeetUpGroups", t => t.MeetUpGroupId, cascadeDelete: true)
                .Index(t => t.MeetUpGroupId);
            
            CreateTable(
                "dbo.MeetUpGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Description = c.String(),
                        //MeetupGroupImage= c.String(),
                        OrganizerId = c.String(maxLength: 128),
                        HomeTown = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.OrganizerId)
                .Index(t => t.OrganizerId);
            
            CreateTable(
                "dbo.MeetUpGroupTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        FullName = c.String(),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.UserNotifications",
                c => new
                    {
                        NotificationId = c.Int(nullable: false),
                        UserId = c.String(nullable: false, maxLength: 128),
                        IsRead = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => new { t.NotificationId, t.UserId })
                .ForeignKey("dbo.Notifications", t => t.NotificationId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.NotificationId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Notifications",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NotificationType = c.Int(nullable: false),
                        UpdatedDateInfo = c.String(),
                        UpdateLocationInfo = c.String(),
                        FullNotification = c.String(),
                        DateTime = c.DateTime(nullable: false),
                        MeetUpId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MeetUps", t => t.MeetUpId, cascadeDelete: true)
                .Index(t => t.MeetUpId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.MeetupGroupsAndTypes",
                c => new
                    {
                        GroupId = c.Int(nullable: false),
                        GroupTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.GroupId, t.GroupTypeId })
                .ForeignKey("dbo.MeetUpGroups", t => t.GroupId, cascadeDelete: true)
                .ForeignKey("dbo.MeetUpGroupTypes", t => t.GroupTypeId, cascadeDelete: true)
                .Index(t => t.GroupId)
                .Index(t => t.GroupTypeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Attendances", "ParticipantId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Attendances", "MeetUpId", "dbo.MeetUps");
            DropForeignKey("dbo.MeetUpGroups", "OrganizerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.UserNotifications", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.UserNotifications", "NotificationId", "dbo.Notifications");
            DropForeignKey("dbo.Notifications", "MeetUpId", "dbo.MeetUps");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.MeetUps", "MeetUpGroupId", "dbo.MeetUpGroups");
            DropForeignKey("dbo.MeetupGroupsAndTypes", "GroupTypeId", "dbo.MeetUpGroupTypes");
            DropForeignKey("dbo.MeetupGroupsAndTypes", "GroupId", "dbo.MeetUpGroups");
            DropIndex("dbo.MeetupGroupsAndTypes", new[] { "GroupTypeId" });
            DropIndex("dbo.MeetupGroupsAndTypes", new[] { "GroupId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.Notifications", new[] { "MeetUpId" });
            DropIndex("dbo.UserNotifications", new[] { "UserId" });
            DropIndex("dbo.UserNotifications", new[] { "NotificationId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.MeetUpGroups", new[] { "OrganizerId" });
            DropIndex("dbo.MeetUps", new[] { "MeetUpGroupId" });
            DropIndex("dbo.Attendances", new[] { "ParticipantId" });
            DropIndex("dbo.Attendances", new[] { "MeetUpId" });
            DropTable("dbo.MeetupGroupsAndTypes");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Notifications");
            DropTable("dbo.UserNotifications");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.MeetUpGroupTypes");
            DropTable("dbo.MeetUpGroups");
            DropTable("dbo.MeetUps");
            DropTable("dbo.Attendances");
        }
    }
}
