namespace Meetup2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPictureToGroup : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MeetUpGroups", "MeetupGroupImage", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MeetUpGroups", "MeetupGroupImage");
        }
    }
}
