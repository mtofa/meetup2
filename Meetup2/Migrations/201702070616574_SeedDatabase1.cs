namespace Meetup2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedDatabase1 : DbMigration
    {
        public override void Up()
        {
            //Users
            Sql(" INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [FullName]) VALUES (N'9bc908a8-1529-4472-9ec1-a1ee1bcb4346', N'c@d.com', 0, N'ALOT9rRyv0i5D+m5V+OY7aEDuQKmovPV6pJXoNNNWOU9Zy0rVc48PmPwVN2P/n4PWA==', N'4780f6c8-18f1-4af0-b518-9168feb6903a', NULL, 0, 0, NULL, 1, 0, N'c@d.com', N'UserCD')");
            Sql(" INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [FullName]) VALUES (N'dd1f1778-cefb-4eff-b1c4-6a835a82206e', N'e@f.com', 0, N'AG2Op3Zi5EF57U/eGkMlkfC+4nfZeGz4jSGmaPnQMJvwBEtAq0aAJK+XJtn9Gfc8Vg==', N'81ba8953-1f7b-4dd3-bb09-c32fe03e91fb', NULL, 0, 0, NULL, 1, 0, N'e@f.com', N'UserEF')");
            Sql(" INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [FullName]) VALUES (N'e8167c24-b882-464e-b272-2ed715a88764', N'a@b.com', 0, N'AHFq86jdEj7MOKf8T0F9SPmkbZWZn3fR8mfe9hGjUaKdOcFLV7WNKCaIjEiOFNYasg==', N'a6cba4cf-7fee-4ca9-b863-81205ae035c9', NULL, 0, 0, NULL, 1, 0, N'a@b.com', N'UserAB')");




          

        }

        public override void Down()
        {
        }
    }
}
