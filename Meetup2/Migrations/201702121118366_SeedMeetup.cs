namespace Meetup2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedMeetup : DbMigration
    {
        public override void Up()
        {
            Sql("SET IDENTITY_INSERT MeetUps ON");
            ////Meetups
            Sql("  INSERT [dbo].[MeetUps] ([Id], [Title], [DateTime], [Location], [Details], [MeetUpGroupId], [IsCancled]) VALUES (21, N'Azure IaaS 101 and What''s new in Windows Server 2016?', CAST(N'2018-02-09T19:00:00.000' AS DateTime), N'Microsoft Melbourne office', N'Synopsis: In this session we will go through a brief history of Azure IaaS and focus on the newest updates from Compute instances, Storage, Networking and ExpressRoute to Azure Site Recovery, Backup and Azure StorSimple.', 1, 0)");
            Sql("  INSERT [dbo].[MeetUps] ([Id], [Title], [DateTime], [Location], [Details], [MeetUpGroupId], [IsCancled]) VALUES (22, N'Working with Azure Table Storage and Developing Your OWN custom OMS solutions', CAST(N'2018-02-09T22:55:00.000' AS DateTime), N'Microsoft Melbourne office', N'Synopsis: In this session, you will learn how to build your own custom OMS solutions using various tools and APIs. We will cover an end-to-end scenario from data injection to custom visual tiles, reports and dashboards.', 1, 0)");
            Sql("  INSERT [dbo].[MeetUps] ([Id], [Title], [DateTime], [Location], [Details], [MeetUpGroupId], [IsCancled]) VALUES (23, N'Deploying, Configuring, and Managing Nano Server and From EMS to EM+S', CAST(N'2018-02-09T19:40:00.000' AS DateTime), N'Microsoft Melbourne office', N'Please join us for this next event for the Melbourne Microsoft Cloud and Datacenter Meetup group. Synopsis: Nano Server requires that IT Pros embrace a new approach to the deployment, configuration, and management of servers and their workloads.Nano Server is the new minimal footprint version of Windows Server 2016 and is likely to represent the future of the Windows Server operating system. ', 1, 0)");
            Sql("  INSERT [dbo].[MeetUps] ([Id], [Title], [DateTime], [Location], [Details], [MeetUpGroupId], [IsCancled]) VALUES (25, N'Evolving your Automation with Hybrid Workers and ConfigMgr in the Cloud', CAST(N'2018-02-09T21:05:00.000' AS DateTime), N'Microsoft Melbourne office', N'Meetup for 2018. Kicking off the new year with the highest rated topics from the recent survey and we have University of Melbourne presenting what they''ve been doing in the Cloud lately.', 1, 0)");
            Sql("  INSERT [dbo].[MeetUps] ([Id], [Title], [DateTime], [Location], [Details], [MeetUpGroupId], [IsCancled]) VALUES (27, N'Big Data & Analytics Innovation Summit - Day 1', CAST(N'2018-02-25T12:20:00.000' AS DateTime), N'Sofitel Melbourne', N'Day 1 of Big Data & Analytics Innovation Summit, a 2 day event hosted by Innovation Enterprise. You must register with the event to book your seat. ', 8, 0)");
            Sql("  INSERT [dbo].[MeetUps] ([Id], [Title], [DateTime], [Location], [Details], [MeetUpGroupId], [IsCancled]) VALUES (28, N'Leveraging Machine Learning for Competitive Advantage... @ BDAI Summit 2018', CAST(N'2018-02-09T12:00:00.000' AS DateTime), N'Sofitel Melbourne', N'The summit will help your business understand & utilize data-driven strategies and discover what disciplines will change because of the advent of data. With a vast amount of data now available, modern businesses are faced with the challenge of storage, management, analysis, privacy, visualization, security and disruptive tools & technologies.', 8, 0)");
            Sql("  INSERT [dbo].[MeetUps] ([Id], [Title], [DateTime], [Location], [Details], [MeetUpGroupId], [IsCancled]) VALUES (29, N'Data Science Transforming How We Manage our Customer Relationships... @ BDAI', CAST(N'2018-02-23T12:25:00.000' AS DateTime), N'Sofitel Melbourne', N'The summit will help your business understand & utilize data-driven strategies and discover what disciplines will change because of the advent of data. With a vast amount of data now available, modern businesses are faced with the challenge of storage, management, analysis, privacy, visualization, security and disruptive tools & technologies.', 8, 0)");

        }

        public override void Down()
        {
        }
    }
}
