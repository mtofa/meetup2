namespace Meetup2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedMeetupGroup : DbMigration
    {
        public override void Up()
        {
            Sql("SET IDENTITY_INSERT MeetUpGroups ON");
            Sql(" INSERT [dbo].[MeetUpGroups] ([Id], [Title], [Description], [OrganizerId], [HomeTown], [MeetupGroupImage]) VALUES (1, N'Melbourne Microsoft Cloud And Datacenter Meetup', N'The successor to the Melbourne System Center, Security, and Infrastructure Group User Group. This is a Meetup Group focused on Microsoft Cloud and Datacenter technologies covering Microsoft Azure, Azure Stack, System Center, Windows Server and Hyper-V and Microsoft Hybrid Cloud technologies.  ', N'e8167c24-b882-464e-b272-2ed715a88764', NULL, N'group1.jpg')");
            Sql(" INSERT [dbo].[MeetUpGroups] ([Id], [Title], [Description], [OrganizerId], [HomeTown], [MeetupGroupImage]) VALUES (7, N'Melbourne Functional User Group (MFUG)', N'We are a group of people interested in functional programming. This ranges from academic discussions around type systems right through to demonstrations of software we''ve developed using practical functional languages. At our meetups we usually have two or three talks and some pizzas before heading out to a local pub for some drinks and more conversation. People of all experience levels are welcome. If you''ve never done FP before and are curious to see what it''s about, or looking to figure out where to start then come along. If you''ve done some FP then we''d love to hear and see what you''ve gotten up to. Finally if you''re a pro please come along and share your knowledge and engage in some invigorating discussions.', N'e8167c24-b882-464e-b272-2ed715a88764', NULL, N'group2.jpg')");
            Sql(" INSERT [dbo].[MeetUpGroups] ([Id], [Title], [Description], [OrganizerId], [HomeTown], [MeetupGroupImage]) VALUES (8, N'Big Data (Next Gen Hadoop) Ingest & Transform, Melbourne', N'This meetup has been formed to discuss customer use cases and design patterns for big data ingestion, transformation, and egression. Use cases for transformations include fast real-time analytics, data movement, disaster recovery in Hadoop, data integration, fast batch, and database load/off-load. Key focus will be on integrating Hadoop with various systems including Message Systems, File Systems, and Databases. Users should be able to connect to any source and any destination from within Hadoop. Ability to leverage any user code as part of the connections and transformations with minimal paradigm limitations. ', N'e8167c24-b882-464e-b272-2ed715a88764', N'Melbourne', N'group3.jpg')");

          
        }

        public override void Down()
        {
        }
    }
}
