﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Meetup2.Startup))]
namespace Meetup2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
