﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Meetup2.Models;
using Microsoft.AspNet.Identity;

namespace Meetup2.Api
{
    [RoutePrefix("api/attendance")]
    public class AttendanceController : ApiController
    {
        private ApplicationDbContext _context;

        public AttendanceController()
        {
                _context = new ApplicationDbContext();
        }

        
        [Route("rsvp")]
        [HttpPost]
        public IHttpActionResult UpdateRsvp(RsvpViewModel rsvpViewModel)
        {
            if (!User.Identity.IsAuthenticated)
                //forbidden will show the login page.
                throw new HttpResponseException(HttpStatusCode.Forbidden);

            var currentUserId = User.Identity.GetUserId();
            var checkRsvp = _context.Attendances
                .FirstOrDefault(a => a.MeetUpId == rsvpViewModel.Id && a.ParticipantId == currentUserId);

            if (checkRsvp==null)
            {
                var attandanceObj = new Attendance()
                {
                    ParticipantId = currentUserId,
                    MeetUpId = rsvpViewModel.Id
                };
                _context.Attendances.Add(attandanceObj);
            }
            else
            {
               _context.Attendances.Remove(checkRsvp);

            }

    
            _context.SaveChanges();


            return Ok(rsvpViewModel.Id);
        }

    }

    public class RsvpViewModel
    {
        public int Id { get; set; }
    }
}
