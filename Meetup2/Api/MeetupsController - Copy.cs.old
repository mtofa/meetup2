﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Meetup2.Models;
using Meetup2.Persistence;
using Meetup2.Repositories;
using Meetup2.ViewModel;
using Microsoft.AspNet.Identity;

namespace Meetup2.Api
{
    [System.Web.Http.RoutePrefix("api/Meetups")]
    public class MeetupsController : ApiController
    {

        //private ApplicationDbContext _context;
        //private MeetupRepository _meetupRepository;
        private IUnitOfWork _unitOfWork;
        private ApplicationDbContext _context;

        const int maxPageSize = 10;

        public MeetupsController(IUnitOfWork unitOfWork)
        {
            _context = new ApplicationDbContext();
            //_meetupRepository = new MeetupRepository(_context);
            _unitOfWork = unitOfWork;
        }

        [System.Web.Http.Route("")]
        [System.Web.Http.HttpGet]
        public IEnumerable<MeetupDto> GetAllMeetups(string query=null,string meetupdate=null,int page=0,int pageSize=4)
        {

            // Thread.Sleep(1000);
            //var meetupQuery = _context.MeetUps
            //    .Where(e => e.DateTime > DateTime.Now);

            var meetupQuery = _unitOfWork.MeetupRepository.GetAllFutureMeetups();

            /*
             *  Search by Date
             */
            if (!String.IsNullOrEmpty(meetupdate))
            {
                DateTime eventdate;
                // if (DateTime.TryParse(meetupdate, out eventdate))
                //if (DateTime.TryParse(meetupdate, out eventdate))
                //Giving fall back format to try parse, else it may fail in other pc
                // if region is different, first try US(default), if dosenot work, use UK

                var formats = new[] { "MM/dd/yyyy", "dd/MM/yyyy" };
                if (DateTime.TryParseExact(meetupdate, formats, CultureInfo.InvariantCulture, DateTimeStyles.None,
                    out eventdate))
                {
                    //meetupQuery = meetupQuery.Where(m =>m.DateTime.Year==eventdate.Year
                    //&&m.DateTime.Month==eventdate.Month
                    //&&m.DateTime.Day==eventdate.Day);

                    meetupQuery = meetupQuery.Where(
                        m => DbFunctions.TruncateTime(m.DateTime) == eventdate.Date);

                    //meetupdate = eventdate.Date.ToString("MM/dd/yyyy");
                }

                else
                {
                    throw new HttpResponseException(HttpStatusCode.NotAcceptable);
                }
            }

            if (!String.IsNullOrEmpty(query))
            {
                //Thread.Sleep(1000);
                meetupQuery = meetupQuery.Where(m => m.Title.Contains(query));
            }

           // var allMeetups = meetupQuery; //.Include(e => e.MeetUpGroup);

            if (pageSize > maxPageSize)
            {
                pageSize = maxPageSize;
            }

            //calculating metadata for header
            var totalCount = meetupQuery.Count();
             // if (totalCount== 0) throw new HttpResponseException(HttpStatusCode.NoContent);

            var totalPages = (int) Math.Ceiling((double) totalCount/pageSize);
            var urlHelper = new UrlHelper(System.Web.HttpContext.Current.Request.RequestContext);
            var prevLink = page > 0 ? urlHelper.Action("", "api/Meetups/", new { meetupdate, page = page - 1, pageSize = pageSize }) : "none";
            var nextLink = page < totalPages - 1 ? urlHelper.Action("", "api/Meetups/", new { meetupdate, page = page + 1, pageSize = pageSize }) : "none";
          
            var paginationHeader = new
            {
                TotalCount = totalCount,
                TotalPages = totalPages,
                PrevPageLink = prevLink,
                NextPageLink = nextLink,
                IsAuthorized = User.Identity.IsAuthenticated
            };

            System.Web.HttpContext.Current.Response.Headers.Add("X-Pagination",
            Newtonsoft.Json.JsonConvert.SerializeObject(paginationHeader));


            var results = meetupQuery
                         .OrderBy(m => m.DateTime)
                         .Skip(pageSize * page)
                         .Take(pageSize)
                         .ToList();

            return results.Select(m => new MeetupDto()
                  {
                    Id = m.Id,
                    Title = m.Title,
                    GroupTitle = m.MeetUpGroup.Title,
                    GroupId = m.MeetUpGroup.Id,
                    DateTime = m.DateTime.ToString("s"),
                    IsCancled = m.IsCancled
                     
            }
                 );



        }

        /*
         * Seperate api call for type ahead. It is NOT 
         * query search!
         */
        [System.Web.Http.Route("search")]
        [System.Web.Http.HttpGet]
        public IEnumerable<SearchDto> SearchMeetups(string query = null,string currentPage=null)
        {
            if (String.IsNullOrEmpty(query))
            {
                throw new HttpResponseException(HttpStatusCode.NotAcceptable);
            }


            if (String.IsNullOrEmpty(currentPage))
            {
                throw new HttpResponseException(HttpStatusCode.NotAcceptable);
            }


            if (currentPage == "groups")
            {
                var meetupgroups = _unitOfWork.MeetupGroupRepository.SearchMeetupTitle(query);
                return meetupgroups.Select(n => new SearchDto()
                {
                    Id = n.Id,
                    Title = n.Title
                });
            }


            var meetup = _unitOfWork.MeetupRepository.SearchMeetupTitle(query);
            return meetup.Select(n => new SearchDto()
            {
                Id = n.Id,
                Title = n.Title
            });

           
           













        }


        //[Authorize]
        [System.Web.Http.Route("myEvents")]
        [System.Web.Http.HttpGet]
        public IEnumerable<MeetupDto> MyEvents(int page = 0, int pageSize = 4)
        {
            
            if (!User.Identity.IsAuthenticated)
                //forbidden will show the login page.
              throw new HttpResponseException(HttpStatusCode.Forbidden); 

            var currentUserId = User.Identity.GetUserId();
            var myAttandance = _unitOfWork.MeetupRepository.GetMyMeetups(currentUserId);

            if (pageSize > maxPageSize)
            {
                pageSize = maxPageSize;
            }

            //calculating metadata for header
            var totalCount = myAttandance.Count();
            var totalPages = (int)Math.Ceiling((double)totalCount / pageSize);
            var urlHelper = new UrlHelper(System.Web.HttpContext.Current.Request.RequestContext);
            var prevLink = page > 0 ? urlHelper.Action("", "api/Meetups/myEvents/", new { page = page - 1, pageSize = pageSize }) : "none";
            var nextLink = page < totalPages - 1 ? urlHelper.Action("", "api/Meetups/myEvents/", new { page = page + 1, pageSize = pageSize }) : "none";

            var paginationHeader = new
            {
                TotalCount = totalCount,
                TotalPages = totalPages,
                PrevPageLink = prevLink,
                NextPageLink = nextLink
            };

            System.Web.HttpContext.Current.Response.Headers.Add("X-Pagination",
            Newtonsoft.Json.JsonConvert.SerializeObject(paginationHeader));


            var result = _unitOfWork.MeetupRepository.GetMyMeetupWithGroup(currentUserId)
                .Skip(pageSize * page)
                .Take(pageSize)
                .ToList();

            return result.Select(m => new MeetupDto()
            {
                Id = m.MeetUpId,
                Title = m.MeetUp.Title,
                GroupTitle = m.MeetUp.MeetUpGroup.Title,
                DateTime = m.MeetUp.DateTime.ToString("s"),
                IsCancled = m.MeetUp.IsCancled,
                IsAuthorized = User.Identity.IsAuthenticated
            }
             );




        }

        [System.Web.Http.Route("Cancle")]
        [System.Web.Http.HttpPost]
        public IHttpActionResult CancleMeetup(MeetupCancleViewModel meetupCancleViewModel)
        {


            var currentUserId = User.Identity.GetUserId();
            var meetups = _unitOfWork.MeetupRepository
                .GetSubscribersByMeetupId(meetupCancleViewModel.Id);
            //_context.MeetUps
            //.Include(g => g.Attendances.Select(a => a.Participant)) //<-- (2)select inside include
            //.SingleOrDefault(m => m.MeetUpGroup.OrganizerId == currentUserId
            //&& m.Id == meetupCancleViewModel.Id);

            if (meetups == null)
                //throw new HttpResponseException(HttpStatusCode.Forbidden);
                return NotFound();

            meetups.CancleAllSubscribers();

            _unitOfWork.Complete();


            return Ok(meetupCancleViewModel.Id);
        }





    }

    public class SearchDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }

    public class MeetupDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string GroupTitle { get; set; }
        public int GroupId { get; set; }
        public byte Participants { get; set; }
        public string DateTime { get; set; }
        public bool IsCancled { get; set; }
        public bool IsAuthorized { get; set; }
    }
}
