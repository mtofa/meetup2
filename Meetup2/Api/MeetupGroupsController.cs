﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Data.Entity;//<-- (1)select inside include
using System.Net.Http;
using System.Web.Http;
using Meetup2.Models;
using Meetup2.ViewModel;
using Microsoft.AspNet.Identity;

namespace Meetup2.Api
{
    [System.Web.Http.RoutePrefix("api/MeetupGroups")]
    public class MeetupGroupsController : ApiController
    {
        private ApplicationDbContext _context;

        public MeetupGroupsController()
        {
            _context = new ApplicationDbContext();
        }


        [System.Web.Http.Route("")]
        [System.Web.Http.HttpGet]
        public IEnumerable<MeetupGroupDto> GetAllMeetupGroups(string query = null, string meetupdate = null, int page = 0,
            int pageSize = 4)
        {



            if (!string.IsNullOrEmpty(query))
            {
                var result = _context.MeetUpGroups
                 .Where(g => g.Title.Contains(query))
                 .ToList();

                return result.Select(n => new MeetupGroupDto()
                {
                    Id = n.Id,
                    Title = n.Title,
                    Details = n.Description,
                    Img = n.MeetupGroupImage
                });
            }

           var allGroups = _context.MeetUpGroups
                .ToList();

           return allGroups.Select(n=>new MeetupGroupDto()
            {
                Id = n.Id,
                Title = n.Title,
                Details = n.Description,
                Img = n.MeetupGroupImage
            });
        }



       
    }

    public class MeetupGroupDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Details { get; set; }
        public string Img { get; set; }
    }
}
