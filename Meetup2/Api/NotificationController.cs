﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Meetup2.Models;
using Microsoft.AspNet.Identity;

namespace Meetup2.Api
{
    [RoutePrefix("api/notification")]
    public class NotificationController : ApiController
    {

        private ApplicationDbContext _context;

        public NotificationController()
        {
            _context = new ApplicationDbContext();
        }

        //[Authorize]
        [Route("byuser")]
        [HttpGet]
        public IEnumerable<NotificationDto> NotificationByUser()
        {

            var currentUserId = User.Identity.GetUserId();

            var notifications = _context.UserNotifications
                .Where(u => u.UserId == currentUserId)
                .Select(u => u.Notification)
                .ToList();

            return notifications.Select(n=>new NotificationDto()
            {
                Id=n.MeetUpId,
                Title = n.FullNotification,
                NotificationType = n.NotificationType,
                MeetupDate =n.UpdatedDateInfo,
                MeetupLocation=n.UpdateLocationInfo
                
            });


        }

    }

    public class NotificationDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public NotificationType NotificationType { get; set; }
        public string MeetupDate { get; set; }
        public string MeetupLocation { get; set; }
    }
}
