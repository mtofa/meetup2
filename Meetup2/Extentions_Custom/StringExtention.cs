﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Meetup2.Extentions_Custom
{
    public static class StringExtention
    {
        public static string Truncate(this string value, byte maxlength)
        {
            return value.Length <= maxlength ? value : value.Substring(0, maxlength) + "....";
        }

    }
}