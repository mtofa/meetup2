﻿using System.Web;
using System.Web.Optimization;

namespace Meetup2
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/angularProjectpkg").Include(
             //  "~/AngularPkg/angular.min.js", //AngularJS v1.4.5
             //  "~/AngularPkg/angular-sanitize.min.js",
               "~/AngularPkg/jcs-auto-validate.min.js", //angular-auto-validate - v1.19.6
               "~/AngularPkg/spin.min.js",
               "~/AngularPkg/ladda.min.js", //Ladda 0.9.8  
               "~/AngularPkg/angular-ladda.min.js",/*! angular-ladda 0.3.1 */
                "~/AngularPkg/ui-bootstrap-tpls-2.4.0.min.js",//angular-ui-bootstrap -v2.4.0
                "~/AngularPkg/angular-bootstrap-confirm.min.js",
                 "~/AngularPkg/angular-ui-router.min.js"
                ));

            //bundles.Add(new ScriptBundle("~/bundles/angularProject").Include(
            //     "~/Js/calander*",
            //   "~/Js/typeAhead.js",
            //    "~/Js/meetup.js",
            //    "~/Js/meetupGroup.js",
            //    "~/Js/app*"
            //    ));

            bundles.Add(new ScriptBundle("~/bundles/angularProject").Include(
                "~/Js/min/all.min.js"
               ));



            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.min.css",
                      "~/Content/font-awesome.min.css",
                      "~/Content/site.css"));
        }
    }
}
