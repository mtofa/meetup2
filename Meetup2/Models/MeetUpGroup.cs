﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;

namespace Meetup2.Models
{
    public class MeetUpGroup
    {
        public  int Id { get; set; }
        public  string Title { get; set; }
        public  string Description { get; set; }
        public  ICollection<MeetUpGroupType> MeetUpGroupTypeses { get; set; }
        public  ApplicationUser Organizer { get; set; }
        public  string OrganizerId { get; set; }
        public  ICollection<MeetUp> MeetUps { get; set; }
        public  string HomeTown { get; set; }
        public  string MeetupGroupImage { get; set; }
        //ROT - Always initialize Collection. 
        public  MeetUpGroup()
        {
            MeetUpGroupTypeses = new Collection<MeetUpGroupType>();
            MeetUps = new Collection<MeetUp>();
        }
    }
}