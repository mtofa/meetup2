﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Web.Services.Description;
using Meetup2.ViewModel;

namespace Meetup2.Models
{
    public class MeetUp
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime DateTime { get; set; }
        public string Location { get; set; }
        public string Details { get; set; }
        public MeetUpGroup MeetUpGroup { get; set; }
        public int MeetUpGroupId { get; set; }
        public bool IsCancled { get; private set; }
        public ICollection<Attendance> Attendances { get; set; }

        public MeetUp()
        {
            Attendances = new Collection<Attendance>();
        }

        public void CancleAllSubscribers()
        {
            this.IsCancled = true;
            var notification = Notification.CancleNotification(this);
            foreach (var participant in this.Attendances.Select(a => a.Participant))
            {
                participant.Notify(notification);

            }
        }

        public void UpdateAllSubscribers(EventViewModel eventViewModel)
        {

            Id = eventViewModel.MeetupId;
            Title = eventViewModel.Title;
            Location = eventViewModel.Location;
            Details = eventViewModel.Details;
            DateTime = eventViewModel.EventDate();

            var notification = Notification.UpdateNotification(this);
            

            foreach (var participant in this.Attendances.Select(a => a.Participant))
            {
                participant.Notify(notification);

            }


        }
    }
}