﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Management;

namespace Meetup2.Models
{
    public class Notification
    {
        public int Id { get; set; }
        public NotificationType NotificationType { get; set; }
        public string UpdatedDateInfo { get; set; }
        public string UpdateLocationInfo { get; set; }
        public string FullNotification { get; set; } //easy to show in Angular
        public DateTime DateTime { get; set; }
        public MeetUp MeetUp { get; set; }
        [Required]
        public int MeetUpId { get; set; }

        public ICollection<UserNotification> UserNotifications { get; set; }

        public Notification()
        {
               
               UserNotifications = new Collection<UserNotification>(); 
        }

        private Notification(NotificationType type, MeetUp meetUp)
        {
            DateTime = DateTime.Now;
            NotificationType = type;
            MeetUp = meetUp;
            FullNotification = meetUp.Title;
        }

        public static Notification CancleNotification(MeetUp meetUp)
        {
            var notification = new Notification(NotificationType.Cancle, meetUp);
            return notification;
        }

        public static Notification UpdateNotification(MeetUp meetUp)
        {
            var notification = new Notification(NotificationType.Update, meetUp);
            notification.UpdateLocationInfo = meetUp.Location;
            notification.UpdatedDateInfo = meetUp.DateTime.ToLongDateString();
            notification.FullNotification = meetUp.Title;
            return notification;
        }


        
    }
}