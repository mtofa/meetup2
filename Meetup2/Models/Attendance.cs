﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Meetup2.Models
{
    public class Attendance
    {

        public MeetUp MeetUp { get; set; }
        public ApplicationUser Participant { get; set; }

        [Key]
        [Column(Order = 1)]
        public int MeetUpId { get; set; }

        [Key]
        [Column(Order=2)]
        public string ParticipantId { get; set; }

    }
    
}