﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Meetup2.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public string FullName { get; set; }

        public ICollection<UserNotification> UserNotifications  { get; set; }

        public ApplicationUser()
        {
            UserNotifications=new Collection<UserNotification>();
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            userIdentity.AddClaim(new Claim("CFullName", FullName));
            
            return userIdentity;
        }

        public void Notify(Notification notification)
        {
           UserNotifications.Add(new UserNotification(this, notification));
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<MeetUpGroup> MeetUpGroups { get; set; }
        public DbSet<MeetUpGroupType> MeetUpGroupTypeses { get; set; }
        public DbSet<MeetUp> MeetUps { get; set; }
        public DbSet<Attendance> Attendances { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<UserNotification> UserNotifications { get; set; }
        
        public ApplicationDbContext()
            : base("MeetupConnection2", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MeetUpGroup>()
                .HasMany(m => m.MeetUpGroupTypeses)
                .WithMany(c => c.MeetUpGroups)
                .Map(m =>
                {
                    m.ToTable("MeetupGroupsAndTypes");
                    m.MapLeftKey("GroupId");
                    m.MapRightKey("GroupTypeId");
                });

            base.OnModelCreating(modelBuilder);
        }
    }
}