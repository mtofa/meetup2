using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Meetup2.Models
{
    public class MeetUpGroupType
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public ICollection<MeetUpGroup> MeetUpGroups { get; set; }

        public MeetUpGroupType()
        {
            MeetUpGroups = new Collection<MeetUpGroup>();
        }
    }
}