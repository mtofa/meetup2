﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Meetup2.Extentions_Custom;
using Meetup2.Models;
using Meetup2.ViewModel;
using Microsoft.AspNet.Identity;

namespace Meetup2.Controllers
{
    [Authorize]
    public class MeetupGroupController : Controller
    {
        private ApplicationDbContext _context;

        public MeetupGroupController()
        {
            _context = new ApplicationDbContext();
        }

        // GET: MeetupGroup
        public ActionResult Index()
        {
            var currentUser = User.Identity.GetUserId();
            var groupInfo = _context.MeetUpGroups
                .Include(u=>u.Organizer)
                .Where(u => u.OrganizerId == currentUser)
                .ToList();
            
            if (!groupInfo.Any())
            {
               
                return RedirectToAction("New");
            }
           
            
            return View(groupInfo);
        }

        [AllowAnonymous]
        public ActionResult Show(int? id)
        {
            if (id == null) return Redirect("~");

            var currentUser = User.Identity.GetUserId();

            var meetupgroup = _context.MeetUpGroups
                .Include(m=>m.Organizer)
                .Single(m => m.Id == id);
                
            var meetups = _context.MeetUps
                .Where(m => m.MeetUpGroupId == id)
                .OrderBy(m => m.DateTime)
                .ToList();


            ViewBag.heading = meetupgroup.Title;

            var viewModel = new GroupViewModel()
            {
                Id = meetupgroup.Id,
                Title = meetupgroup.Title,
                Description = meetupgroup.Description,
                MeetUp = meetups,
                ShowButton = (currentUser== meetupgroup.Organizer.Id)?true:false


            };

            return View(viewModel);
        }
        public ActionResult New()
        {
            ViewBag.heading = "Start a new Meetup";
            return View();
        }

    
        public ActionResult Create(GroupViewModel viewModel)
        {
            var currentUser = User.Identity.GetUserId();
            var group = new MeetUpGroup()
            {
                Title = viewModel.Title,
                HomeTown = viewModel.HomeTown,
                Description = viewModel.Description,
                OrganizerId = currentUser
            };

            _context.MeetUpGroups.Add(group);
           // _context.SaveChanges();

            return RedirectToAction("Index");

        }
    }
}