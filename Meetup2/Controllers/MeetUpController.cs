﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Meetup2.Models;
using Meetup2.Persistence;
using Meetup2.Repositories;
using Meetup2.ViewModel;
using Microsoft.AspNet.Identity;

namespace Meetup2.Controllers
{
    [Authorize]
    public class MeetUpController : Controller
    {
     
        //private ApplicationDbContext _context;
        private IUnitOfWork _unitOfWork;
    
        public MeetUpController(IUnitOfWork unitOfWork)
        {
            //_context= new ApplicationDbContext();
            //_unitOfWork =new UnitOfWork(new ApplicationDbContext());
            _unitOfWork= unitOfWork;
        }

        public ActionResult Index()
        {
            var currentUser = User.Identity.GetUserId();
            var eventLists = _unitOfWork
                             .MeetupGroupRepository.GetMeetupGroups(currentUser);

            /*
             *  Conditional Include
             *  By default, you cannnot select inside include.
             *  wrong - add "using System.Data.Entity;" and 
             *  Include inside select will work.
             *  Example - meetup group controller.
             *  Evernote - conditional include
             */
            //var dbQuery = from MeetUpGroup in _context.MeetUpGroups
            //              where MeetUpGroup.OrganizerId == currentUser
            //              select new
            //              {
            //                  MeetUpGroup,
            //                  Events = from MeetUp in MeetUpGroup.MeetUps
            //                           where MeetUp.IsCancled == false
            //                           select MeetUp

            //              };
            //var eventLists = dbQuery.AsEnumerable().Select(g => g.MeetUpGroup);
            var viewModel = new EventIndexViewModel()
            {
                MeetUpGroups = eventLists
            };

            return View(viewModel);
        }
        
        public ActionResult New(int id)
        {
            var currentUser = User.Identity.GetUserId();
            var groupInfo = _unitOfWork.MeetupGroupRepository
                .GetCurrentUsersMeetupGroupById(currentUser,id);

            if (groupInfo == null)
                return  RedirectToAction("new", "MeetupGroup");
   
         
            var viewModel = new EventViewModel()
            {
               MeetUpGroupId = id,
               DispalyTitle = groupInfo.Title

            };
            ViewBag.heading = groupInfo.Title;
            return View("EventForm", viewModel);
        }


        [ValidateAntiForgeryToken]
        public ActionResult Create(EventViewModel eventViewModel)
        {
            var currentUser = User.Identity.GetUserId();

            if (!ModelState.IsValid)
            {
                ViewBag.heading = eventViewModel.DispalyTitle;
                return View("EventForm", eventViewModel);
            }


            var eventinfo = new MeetUp()
            {
                Title = eventViewModel.Title,
                Location = eventViewModel.Location,
                Details = eventViewModel.Details,
                DateTime = eventViewModel.EventDate(),
                MeetUpGroupId = eventViewModel.MeetUpGroupId
            };

            
            _unitOfWork.MeetupRepository.Add(eventinfo);
            _unitOfWork.Complete();

            return RedirectToAction("Show", "MeetupGroup",new {id= eventViewModel.MeetUpGroupId});
        }

        public ActionResult Edit(int id)
        {

            //var currentUser = User.Identity.GetUserId();
            //var meetupLists = _context.MeetUpGroups
            //    .Where(m => m.OrganizerId == currentUser)
            //    .ToList();
            var eventInfo = _unitOfWork.MeetupRepository.GetMeetupById(id);

            if (eventInfo == null)
                return null;

            var viewModel = new EventViewModel()
            {
                MeetupId = eventInfo.Id,
                Title = eventInfo.Title,
                Location = eventInfo.Location,
                Details = eventInfo.Details,
                EventDateTime = eventInfo.DateTime.ToString("dd MMMM yyyy - hh:mm tt")
          
            };

            return View("EventForm", viewModel);



        }


        [ValidateAntiForgeryToken]
        public ActionResult Update(EventViewModel eventViewModel)
        {
            var currentUser = User.Identity.GetUserId();



            if (!ModelState.IsValid)
            {

                return View("EventForm", eventViewModel);
            }

            var eventInfo = _unitOfWork.MeetupRepository
                .GetSubscribersByMeetupId(eventViewModel.MeetupId);

            /*
             * It only creates row in notification table if there are subscribers.
             * 
             */
            eventInfo.UpdateAllSubscribers(eventViewModel);


            _unitOfWork.Complete();

            return RedirectToAction("Show", "MeetUpGroup",new {id=eventInfo.MeetUpGroupId});
        }

        
       
        [AllowAnonymous]
        public ActionResult Show(int id)
        {
            //if (id == null)
            //{
            //    return Redirect("~");
            //}
               
            var currentUserId = User.Identity.GetUserId();
            var eventInfo = _unitOfWork.MeetupRepository.GetMeetupById(id);

            var attandanceInfo = _unitOfWork.AttandanceRepository.GetAttendancesById(currentUserId).ToList()
                .ToLookup(a=>a.MeetUpId);

            var viewModel = new EventShowViewModel()
            {
                MeetUp = eventInfo,
                AttandanceLookup = attandanceInfo
            };


            ViewBag.RightLayoutVar = "show";
            ViewBag.heading = eventInfo.MeetUpGroup.Title;
            return View(viewModel);


        }

        [AllowAnonymous]
        public ActionResult RSVP()
        {
            var previousUrl = Request.UrlReferrer.AbsolutePath;

            return  new RedirectToRouteResult(
                new System.Web.Routing.RouteValueDictionary
                    {
                            { "controller", "Account" },
                            { "action", "Login" },
                            { "returnUrl", previousUrl }
                    });
        
        }
    }


}