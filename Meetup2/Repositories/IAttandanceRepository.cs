﻿using System.Linq;
using Meetup2.Models;

namespace Meetup2.Repositories
{
    public interface IAttandanceRepository
    {
        IQueryable<Attendance> GetAttendancesById(string currentUserId);
    }
}