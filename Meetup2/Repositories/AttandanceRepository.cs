﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Meetup2.Models;

namespace Meetup2.Repositories
{
    public class AttandanceRepository : IAttandanceRepository
    {
        private readonly ApplicationDbContext _context;

        public AttandanceRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IQueryable<Attendance> GetAttendancesById(string currentUserId)
        {
            return _context.Attendances
                .Where(a => a.ParticipantId == currentUserId && a.MeetUp.DateTime > DateTime.Now);

        }
    }
}