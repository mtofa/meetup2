﻿using System.Collections.Generic;
using Meetup2.Models;

namespace Meetup2.Repositories
{
    public interface IMeetupGroupRepository
    {
        MeetUpGroup GetCurrentUsersMeetupGroupById(string currentUser, int id);
        IEnumerable<MeetUpGroup> GetMeetupGroups(string currentUser);
        IEnumerable<MeetUpGroup> SearchMeetupTitle(string query);
    }
}