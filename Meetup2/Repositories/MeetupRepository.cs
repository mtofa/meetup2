﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Meetup2.Models;

namespace Meetup2.Repositories
{
    public class MeetupRepository : IMeetupRepository
    {
        private readonly ApplicationDbContext _context;

        public MeetupRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public void Add(MeetUp meetUp)
        {
            _context.MeetUps.Add(meetUp);
        }

        public IEnumerable<Attendance> GetMyMeetups(string currentUserId)
        {
            return _context.Attendances
                .Where(a => a.ParticipantId == currentUserId)
                .Include(a => a.MeetUp);
        }

        public IEnumerable<Attendance> GetMyMeetupWithGroup(string currentUserId)
        {
            return _context.Attendances
                .Where(a => a.ParticipantId == currentUserId)
                .Include(a => a.MeetUp)
                .Include(a => a.MeetUp.MeetUpGroup)
                .OrderBy(a => a.MeetUp.DateTime);
        }

        public MeetUp GetMeetupById(int id)
        {
            return _context.MeetUps
                .Include(e => e.MeetUpGroup)
                .SingleOrDefault(e => e.Id == id);
        }

        public MeetUp GetSubscribersByMeetupId(int meetupId)
        {
            return _context.MeetUps
                .Include(e => e.Attendances.Select(a => a.Participant))
               .Single(e => e.Id == meetupId);
        }

        public IEnumerable<MeetUp> GetAllFutureMeetups()
        {
            return _context.MeetUps
                .Include(e=>e.MeetUpGroup)
                .Where(e => e.DateTime > DateTime.Now);
        }
        public IQueryable GetOnlyFutureMeetups()
        {
            return _context.MeetUps
                  .Where(e => e.DateTime > DateTime.Now);
        }

        public IEnumerable<MeetUp> SearchMeetupTitle(string query)
        {
            return _context.MeetUps
                   .Where(m => m.Title.Contains(query))
                   .ToList();
        }

        public void DeleteMeetupsByTitle(string title)
        {

            var meetups = _context.MeetUps
                .Where(m => m.Title == title)
                .ToList();
            _context.MeetUps.RemoveRange(meetups);


        }


       
    }
}