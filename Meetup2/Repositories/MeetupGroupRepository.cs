﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Meetup2.Models;

namespace Meetup2.Repositories
{
    public class MeetupGroupRepository : IMeetupGroupRepository
    {
        private readonly ApplicationDbContext _context;

        public MeetupGroupRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<MeetUpGroup>GetMeetupGroups(string currentUser)
        {
            return _context.MeetUpGroups
                .Where(g => g.OrganizerId == currentUser)
                .Include(e => e.MeetUps)
                .ToList();
        }

        public MeetUpGroup GetCurrentUsersMeetupGroupById(string currentUser, int id)
        {
           return _context.MeetUpGroups
                .SingleOrDefault(u => u.OrganizerId == currentUser && u.Id == id);

        }
        public IEnumerable<MeetUpGroup> SearchMeetupTitle(string query)
        {
            return _context.MeetUpGroups
                    .Where(m => m.Title.Contains(query))
                    .ToList();
        }
    }
}