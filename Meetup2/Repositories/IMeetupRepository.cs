﻿using System.Collections.Generic;
using System.Linq;
using Meetup2.Models;

namespace Meetup2.Repositories
{
    public interface IMeetupRepository
    {
        void Add(MeetUp meetUp);
        IEnumerable<MeetUp> GetAllFutureMeetups();
        MeetUp GetMeetupById(int id);
        IEnumerable<Attendance> GetMyMeetups(string currentUserId);
        IEnumerable<Attendance> GetMyMeetupWithGroup(string currentUserId);
        MeetUp GetSubscribersByMeetupId(int meetupId);
        IEnumerable<MeetUp> SearchMeetupTitle(string query);
        void DeleteMeetupsByTitle(string title);
        IQueryable GetOnlyFutureMeetups();
    }
}