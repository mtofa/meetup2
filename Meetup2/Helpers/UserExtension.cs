﻿using System.Security.Claims;
using System.Security.Principal;
using Microsoft.AspNet.Identity;

namespace Meetup2.Helpers.Users
{
    public static class AnynameExtended
    {
        public static string GetFullName(this IPrincipal user)
        {
            var claim = ((ClaimsIdentity)user.Identity).FindFirstValue("CFullName");
            return claim ?? (string)null;
        }
    }

    
    
}