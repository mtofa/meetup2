﻿angular.module("MeetUp", ['Meetup', 'CalanderModule', 'AutoSelectModule', 'MeetupGroup', "RouterModule"]);

var routerApp = angular.module("RouterModule", ['ui.router']);
routerApp.config(function ($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state("calender",
        {
            url: "/",
            views: {
                'main': {
                    templateUrl: "/Templates/Home/Index.html"
                    //controller: "MeetupListController"
                },
                'searchview': {
                    templateUrl: "/Templates/Home/Search.html"
                    //controller: "MeetupListController"
                }
            }
           
        })
        .state("groups",
        {
            url: "/groups",
            views: {
                'main': {
                    templateUrl: "/Templates/MeetupGroup/Index.html"
                    // controller: "MeetupListController" 
                },
                'searchview': {
                    templateUrl: "/Templates/Home/Search.html"
                    //controller: "MeetupListController"
                }
            }
          
        
        });

    $urlRouterProvider.otherwise('/');

});