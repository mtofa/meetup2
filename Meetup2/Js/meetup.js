var appMeetup = angular.module('Meetup', ['jcs-autoValidate', 'angular-ladda', 'ui.bootstrap']);
//appMeetup.config(function ($httpProvider) {
//    $httpProvider.default.header.common['Authorization'] = "Token 1234";
//});
appMeetup.service("MeeupService", function ($http) {
    this.updateRsvp = function (val) {
        var response = $http({
            method: "post"
            , url: "/api/attendance/rsvp/"
            , data: JSON.stringify(val)
            , dataType: "json"
        });
        return response;
    };
    this.GetNotifications = function () {
        var response = $http.get("/api/notification/byuser/");
        return response;
    };
    this.GetMeetups = function () {
        var response = $http.get("/api/Meetups/");
        return response;
    };
});
appMeetup.service("FrontMeetupService", function ($http, $q) {
    var meethupList = [];
    var nextUrl = [];
    var authenticationStatus=[];
    var loadNextUrl = function (url) {
        if (url[0] !== 'none') angular.copy(url, nextUrl);
        else {
            angular.copy(null, nextUrl);
        }
    };

    var isAuthorized = function(val) {
        angular.copy(val, authenticationStatus);
        //shouldShow = val;

    };

    var loadMeetup = function (userdata,refresh) {

        //If not Push, empry the array first.
        if (refresh)
        meethupList.length = 0;

        angular.forEach(userdata,
               function (meetup) {
                   meethupList.push(meetup);
               });


    };

  
    var fetchAllMeetups = function () {
        var deferred = $q.defer();
        var response = $http.get("/api/Meetups/");
        response.success(function (result, status, headers, config) {
            
            loadMeetup(result,true);
            var headerInfo = $.parseJSON(headers()['x-pagination']);
            loadNextUrl([headerInfo['NextPageLink']]);
            isAuthorized([headerInfo['IsAuthorized']]);
            deferred.resolve();
        }).error(function () {
            deferred.reject();
        });
        return deferred.promise;
    };
    var fetchmyMeetups = function () {
        var deferred = $q.defer();
        var response = $http.get("/api/Meetups/myEvents");
        response.success(function (result, status, headers, config) {
             loadMeetup(result,true);
          
            var headerInfo = $.parseJSON(headers()['x-pagination']);
            loadNextUrl([headerInfo['NextPageLink']]);
            deferred.resolve();
        }).error(function () {
            deferred.reject();
        });
        return deferred.promise;
    };
    var fetchMeetupsByDate = function (val) {
        var deferred = $q.defer();
        var response = $http.get("/api/Meetups?meetupdate=" + val);
        response.success(function (result, status, headers, config) {
            loadMeetup(result, true);
            var headerInfo = $.parseJSON(headers()['x-pagination']);
            loadNextUrl([headerInfo['NextPageLink']]);
            deferred.resolve();
        }).error(function () {
            deferred.reject();
     });
        return deferred.promise;
    };
    var fetchMeetupsByQuery = function (val) {
        var deferred = $q.defer();
        var response = $http.get("/api/Meetups?query=" + val);
        response.success(function (result, status, headers, config) {
            loadMeetup(result,true);
            var headerInfo = $.parseJSON(headers()['x-pagination']);
            loadNextUrl([headerInfo['NextPageLink']]);
            deferred.resolve();
        }).error(function () {
            deferred.reject();
            //console.log("error:/api/Meetups/");
        });
        return deferred.promise;
    };



    var showNextMeetup = function (val) {
        var deferred = $q.defer();
        var response = $http.get(val);
        response.success(function (result, status, headers, config) {

            loadMeetup(result,false);
            var headerInfo = $.parseJSON(headers()['x-pagination']);
            loadNextUrl([headerInfo['NextPageLink']]);
            deferred.resolve();
        }).error(function () {
            deferred.reject();
            //console.log("error:/api/Meetups/");
        });
        return deferred.promise;
    };
    return {
        fetchAllMeetups: fetchAllMeetups
        , fetchmyMeetups: fetchmyMeetups
        , Meetups: meethupList
        , fetchMeetupsByDate: fetchMeetupsByDate
        , fetchMeetupsByQuery: fetchMeetupsByQuery
        , nextUrl: nextUrl
        , showNextMeetup: showNextMeetup
        , loadNextUrl: loadNextUrl
        , authenticationStatus: authenticationStatus
 
    };
});
appMeetup.controller("UserController", function ($scope, MeeupService) {
    $scope.RsvpLaddaStatus = false;
    $scope.RsvpButtonStatus = false;
    $scope.OnRSVP = function (value) {
      //  console.log(value);
        $scope.RsvpLaddaStatus = true;
        
        var rsvpJson = {
            Id: value
        };


        var response = MeeupService.updateRsvp(rsvpJson);
        response.success(function () {
    
            $scope.RsvpLaddaStatus = false;

            if ($scope.RsvpButtonText === "Going") {
                $scope.RsvpButtonText = "RSVP";
                $scope.RsvpbuttonClass = "btn-primary";
            }
            else {
                $scope.RsvpButtonText = "Going";
                $scope.RsvpbuttonClass = "btn-success";
            }
        }).error(function () {
            window.location = "/Account/Login";
            console.log("An error has occured!");
        });
      
    };
});
appMeetup.controller('NotificationCtrl', function ($scope, MeeupService) {
    $scope.elements = [];
    var data;
    $scope.badge_status = true;
    //var init = function () {
        var response = MeeupService.GetNotifications();
        response.success(function (msg) {
            data = msg;
            $scope.NotificationCount = msg.length;
            if ((msg.length) > 0) {
                $scope.badge_status = false;
            }
            else {
                $scope.dynamicPopover.templateUrl = '';
            }
        }).error(function () {});
   // };
  //  init();
    $scope.ShowNotification = function () {
        // console.log(data);
        $scope.elements = data;
    };
    $scope.dynamicPopover = {
        // content: 'Hello, World!',
        templateUrl: 'myPopoverTemplate.html'
        , title: 'Notifications'
    };
});
appMeetup.controller("MeetupListController", function ($scope, $filter,$stateParams,$state, FrontMeetupService) {
   // console.log($stateParams);
   // console.log("state"+$state);
    $scope.data = [];
    //$scope.meetupDate = "";
    var previousdate = "";
    var i = 0;
    $scope.fa_spin_status = false;
    $scope.checkIsMeetupEmpty = function () {
        if ($scope.data.Meetups && $scope.data.Meetups.length <= 0) return false;
        return true;
    };
   // var init = function () {
      
    FrontMeetupService.fetchAllMeetups().then(function () {
        //console.log(FrontMeetupService);
            $scope.data = FrontMeetupService;
        }, function () {
            //error
            alert("Cannot load data...");
        }).then(function () {
            $scope.fa_spin_status = true;
        });
   // };
   // init(); //<---call first time
    //Filter function
    $scope.shouldShowDate = function (mdate, index) {
        var getDate = $filter('date')(mdate, "dd/MM/yyyy");
        if (index === 0) {
            // console.log(index, '-->yes');
            previousdate = getDate;
            return true;
        }
        if (getDate === previousdate) {
            return false;
        }
        else {
            previousdate = getDate;
            return true;
        }
    };
    $scope.showNext = function (url) {
        FrontMeetupService.showNextMeetup(url);
    };
    $scope.shouldShowNextButton = function (nexturl, data) {
        //   if (data&& data.length <= 0) return false;
        if (nexturl && nexturl.length > 0) {
            return true;
        }
        return false;
    };
    $scope.shouldShowMessage = function () {
        if ($scope.data.Meetups.length > 0) return false;
        return true;
    };

   
});
appMeetup.controller("MeetupNavController", function ($scope, $filter, FrontMeetupService) {
    $scope.data = [];
    var previousdate = "1/1/20";
    var i = 0;
    $scope.fa_spin_status = false;
    $scope.data = FrontMeetupService;
    $scope.shouldShowMenu = function(val) {
        //console.log(val[0]);
        if (val[0]&&val[0]===true) return true;
        return false;
        //return true;
    };

    $scope.showAllMeetups = function () {
        $scope.selectedMenu = 'AllMeetups';
        
        FrontMeetupService.fetchAllMeetups().then(function () {
            //FrontMeetupService.emptyMeetups();
            //success
           // console.log(FrontMeetupService);
        }, function () {
            //error
            console.log("Cannot load - $scope.showAllMeetups ");
        }).then(function () {
            $scope.fa_spin_status = true;
        });
    }
    $scope.showIamGoing = function () {
            $scope.selectedMenu = 'IamGoing';
        FrontMeetupService.fetchmyMeetups()
            .then(function () {
                //console.log("here ...");
                //success
                //$scope.data = FrontMeetupService;
            }, function (msg) { // fail
                //api is sending 404,forbiden if not authenticated.
                window.location = "/Account/Login";
            }).then(function () {
                $scope.fa_spin_status = true;
            });
        }
        //Filter function
    $scope.shouldShowDate = function (mdate) {
        var getDate = $filter('date')(mdate, "dd/MM/yyyy");
        if (getDate === previousdate) {
            return false;
        }
        else {
            previousdate = getDate;
            return true;
        }
    }
});