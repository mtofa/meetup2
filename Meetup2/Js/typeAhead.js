﻿var autoSelectApp = angular.module('AutoSelectModule', ['ngSanitize', 'ui.bootstrap', 'Meetup']);

autoSelectApp.controller('TypeaheadCtrl', function ($scope, $http, $state, FrontMeetupService, meetupGroupService) {

    var _selected;
    var currentUrl="calendar";

    $scope.selected = undefined;
    $scope.getMeetups = function (val) {
        var titles = [];
        currentUrl = $state.href($state.current.name, $state.params, { absolute: false, inherit: false });
        //console.log(currentUrl);

        if (currentUrl.indexOf("groups")>=0) {
            currentUrl = "groups";
        }
        else {
            currentUrl = "calendar";
        }
        

        return $http
 
            .get('/api/Meetups/search?query=' + val+"&currentPage=" + currentUrl).then(function (response) {

                
                $.each(response.data, function (key, value) {
                    titles.push(value.Title);
                });
                
                
                return titles;
                
            });
    };

    $scope.SearchMeetups = function() {
       
        var val = $scope.asyncSelected;
        currentUrl = $state.href($state.current.name, $state.params, { absolute: false, inherit: false });
        //console.log(currentUrl);

        if (currentUrl.indexOf("groups") >= 0) {
            meetupGroupService.fetchMeetupsGroupsByQuery(val).then(function () {
                //success


            }, function () {
                //error
                console.log("Error ....SearchMeetups");
            }).then(function () {

            });
        }
        else {
            FrontMeetupService.fetchMeetupsByQuery(val).then(function () {
                //success


            }, function () {
                //error
                console.log("Error ....SearchMeetups");
            }).then(function () {

            });
        }

        
      
       




    };

    //$scope.$watch('asyncSelected', function (newValue, oldValue) {

    //    /*
    //     *  First need to check whether the newVAlue exists or not 
    //     *  then check the lenght. Else, it will show error, 
    //     *  Cannot read property 'length' of undefined
    //     *  
    //     */
    //    if (newValue && newValue.length > 3) {
    //        FrontMeetupService.fetchMeetupsByQuery(newValue);
    //    }
    //   // console.log(newValue);
    //});
});



/*
 *  Script automatically query address in google
 */

//autoSelectApp.controller('TypeaheadCtrl', function ($scope, $http) {

//    var _selected;

//    $scope.selected = undefined;
//    //  $scope.states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California'];
//    // Any function returning a promise object can be used to load values asynchronously
//    $scope.getLocation = function (val) {
//        return $http.get('//maps.googleapis.com/maps/api/geocode/json', {
//            params: {
//                address: val,
//                sensor: false
//            }
//        }).then(function (response) {
//            return response.data.results.map(function (item) {
//                return item.formatted_address;
//            });
//        });
//    };

//});