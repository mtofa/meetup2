﻿var appMeetupGroup = angular.module('MeetupGroup', ['mwl.confirm']);

appMeetupGroup.service("meetupGroupService",function($http,$q) {


    var meetupGroups = [];

    var loadGroups = function(result,loadstatus) {

        if (loadstatus) meetupGroups.length = 0;

        angular.forEach(result, function (meetupGroup) {

            meetupGroups.push(meetupGroup);

        });

    };

    var updateEventStatus = function(value) {
        
        var response = $http({
            method: "post"
            , url: "/api/Meetups/Cancle"
            , data: JSON.stringify(value)
            , dataType: "json"
        });
        return response;


    };


    var fetchAllGroups = function () {
        var deferred = $q.defer();
        var response = $http.get("/api/MeetupGroups/");
        response.success(function (result, status, headers, config) {
            //console.log(result);
            loadGroups(result, true);
            //var headerInfo = $.parseJSON(headers()['x-pagination']);
            //loadNextUrl([headerInfo['NextPageLink']]);
            deferred.resolve();
        }).error(function () {
            deferred.reject();
        });
        return deferred.promise;
    };

    var fetchMeetupsGroupsByQuery = function (val) {
        var deferred = $q.defer();
        var response = $http.get("/api/MeetupGroups?query=" + val);
        response.success(function (result, status, headers, config) {
            loadGroups(result, true);
            //var headerInfo = $.parseJSON(headers()['x-pagination']);
            //loadNextUrl([headerInfo['NextPageLink']]);
            deferred.resolve();
        }).error(function () {
            deferred.reject();
            //console.log("error:/api/Meetups/");
        });
        return deferred.promise;
    };

    return {
        updateEventStatus: updateEventStatus,
        fetchAllGroups: fetchAllGroups,
        meetupGroups: meetupGroups,
        fetchMeetupsGroupsByQuery:fetchMeetupsGroupsByQuery

    }

});


appMeetupGroup.controller("appMeetupGroupCtrl", function ($scope, meetupGroupService) {
    $scope.cancleMessage = false;
    $scope.CancleMeetup = function (value) {
        var cancleJson = {
            Id: value
        };


        var response = meetupGroupService.updateEventStatus(cancleJson);
        response.success(function () {
           location.reload(true);

        }).error(function () {
            
            console.log("An error has occured!");
        });
    };
});


appMeetupGroup.controller("MeetupGroupFrontpageCtrl", function ($scope, meetupGroupService) {

    $scope.data = [];
    $scope.test = "sdfdsgfd";

    meetupGroupService.fetchAllGroups().then(function () {
        $scope.data = meetupGroupService;
    }, function () {
        //error
        alert("Error ....");
    }).then(function () {
        $scope.fa_spin_status = true;
    });


});
