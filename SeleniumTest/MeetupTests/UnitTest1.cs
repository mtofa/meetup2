﻿using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SeleniumTest.MeetupTests
{
    [TestClass]
    public class UnitTest1
    {

        //[TestInitialize]
        //public void cleanupDb()
        //{
           

        //}

       

        [TestMethod]
        public void MeetupTests()
        {
            //Test 1: An organizer can create a meet up.
            An_Organizer_Can_Add_A_Meetup();
            Thread.Sleep(600);
            //Test 2: A member can join a meet up.
            An_User_Can_Join_A_Meetup();
            Thread.Sleep(600);
            //Test 3: An organizer can cancel a meet up.
            Organizer_Cancle_A_Meetup();
            Thread.Sleep(600);
            //Test 4: A member can read the cancel notification.
            An_User_Can_Read_Cancle_Notification();

            Browser.Driver.Close();
        }

        

        private void An_Organizer_Can_Add_A_Meetup()
        {

            Pages.HomePage.Goto();
            Pages.HomePage.Maximize();
            Pages.HomePage.ClickLogin();
            Pages.HomePage.EnterCredentials("a@b.com", "Asdfg@1234");
            Pages.HomePage.SelectAMeetup();
            Pages.HomePage.AddaMeetup();
            Pages.HomePage.ClickLogOff();

        }

        private void An_User_Can_Join_A_Meetup()
        {

            Pages.HomePage.Goto();
            Pages.HomePage.Maximize();
            Pages.HomePage.ClickLogin();
            Pages.HomePage.EnterCredentials("e@f.com", "Asdfg@1234");
            Pages.HomePage.JoinAMeetup();
            Pages.HomePage.ClickLogOff();

        }

        private void Organizer_Cancle_A_Meetup()
        {

            Pages.HomePage.Goto();
            Pages.HomePage.Maximize();
            Pages.HomePage.ClickLogin();
            Pages.HomePage.EnterCredentials("a@b.com", "Asdfg@1234");
            Pages.HomePage.SelectAMeetup();
            Pages.HomePage.CancleaMeetup();
            Pages.HomePage.ClickLogOff();

        }


       
        private void An_User_Can_Read_Cancle_Notification()
        {
            Pages.HomePage.Goto();
            Pages.HomePage.Maximize();
            Pages.HomePage.ClickLogin();
            Pages.HomePage.EnterCredentials("e@f.com", "Asdfg@1234");
            Pages.HomePage.ReadNotification();
        }
        private void Can_Go_To_HomePage()
        {

            Pages.HomePage.Goto();
            Assert.IsTrue(Pages.HomePage.IsAt());


        }


    }
}



