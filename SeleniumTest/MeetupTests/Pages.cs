using OpenQA.Selenium.Support.PageObjects;

namespace SeleniumTest.MeetupTests
{
    public static class Pages
    {
        public static  HomePage HomePage
        {
            get
            {
                var HomePage = new HomePage();
                PageFactory.InitElements(Browser.Driver,HomePage);
                return HomePage;
            }
        }
    }
}