using OpenQA.Selenium;

namespace SeleniumTest.MeetupTests
{
    public static class WebdriverExtention
    {
        public static bool HasElement(this IWebDriver driver, By by)
        {
            try
            {
                driver.FindElement(by);
            }
            catch (NoSuchElementException ex)
            {
                return false;
            }

            return true;
        }
    }
}