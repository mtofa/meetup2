﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace SeleniumTest.MeetupTests
{
    public static class Browser
    {
        static IWebDriver webDriver = new ChromeDriver();

        public static string Title
        {
            get { return webDriver.Title; }
        }

        public static IWebDriver Driver { get {return webDriver;} }

        public static void Goto(string url)
        {

            webDriver.Url = url;
            

        }
        public static void FillTextByClass(string className, string text)
        {
            GetByClass(className).Clear();
            GetByClass(className).SendKeys(text);
        }

        public static void FillTextByName(string name, string text)
        {
            GetByName(name).Clear();
            GetByName(name).SendKeys(text);
        }
        public static void ClickLink(string text)
        {
            GetByLink(text).Click();
        }
        public static void ClickName(string text)
        {
            GetByName(text).Click();
        }

        public static void ClickClass(string className)
        {
            GetByClass(className).Click();
        }

        public static void ClickXpath(string xpath)
        {
            GetByXpath(xpath).Click();
        }
        public static IWebElement GetByXpath(string xpath)
        {
            return
                Browser.Driver.FindElement(By.XPath(xpath));
        }

        public static IWebElement GetByLink(string text)
        {
            return
                Browser.Driver.FindElement(By.LinkText(text));
        }

        public static IWebElement GetByClass(string text)
        {
            return
                Browser.Driver.FindElement(By.ClassName(text));
        }

        public static IWebElement GetByName(string text)
        {
            return
                Browser.Driver.FindElement(By.Name(text));
        }

        public static void Close()
        {
            webDriver.Close();
        }
    }
}