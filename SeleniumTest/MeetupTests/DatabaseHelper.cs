﻿using Meetup2.Persistence;

namespace SeleniumTest.MeetupTests
{
    public class DatabaseHelper 
    {
        private IUnitOfWork _iWork;

        public DatabaseHelper(IUnitOfWork iUnitOfWork)
        {
            _iWork = iUnitOfWork;
        }

        public void RestMeetupTable(string title)
        {
            _iWork.MeetupRepository.DeleteMeetupsByTitle(title);
            _iWork.Complete();
        }

    
    }
}