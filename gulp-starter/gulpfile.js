var gulp=require("gulp");
var uglify=require("gulp-uglify");
var ngAnnotate=require("gulp-ng-annotate");
var rename=require("gulp-rename");
var concat = require("gulp-concat");

gulp.task('minify',function(){
    return gulp.src("E:/Meetup2/Meetup2/Js/*.js")
    .pipe(concat("all.js"))
    .pipe(ngAnnotate())
    .pipe(uglify())
    .pipe(rename("all.min.js"))
    .pipe(gulp.dest("E:/Meetup2/Meetup2/Js/min/"))
});