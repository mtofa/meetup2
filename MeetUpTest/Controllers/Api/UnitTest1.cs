﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Security.Claims;
using System.Security.Principal;
using System.Web.Http.Results;
using FluentAssertions;
using Meetup2.Api;
using Meetup2.Controllers;
using Meetup2.Persistence;
using Meetup2.Repositories;
using Meetup2.ViewModel;
using MeetUpTest.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;


namespace MeetUpTest.Controllers.Api
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class MeetupControllerTests
    {

        private MeetupsController _controller;

        public MeetupControllerTests()
        {
           var mockRepository = new Mock<IMeetupRepository>();
           var mockUoW=new Mock<IUnitOfWork>();

            mockUoW.SetupGet(u => u.MeetupRepository).Returns(mockRepository.Object);

           _controller = new MeetupsController(mockUoW.Object);

           _controller.MockCurrentUser("1","a@b.com");

        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void Cancle_NoMeetupWithGivenIdExists_ShouldReturnNotFound()
        {
            var viewmodel = new MeetupCancleViewModel();
            viewmodel.Id = 12;
            var result = _controller.CancleMeetup(viewmodel);
            result.Should().BeOfType<NotFoundResult>();
        }
    }
}
